/*
** Функции для заполнения анкеты бренда пользователем.
*/
$(function () {
    var app = new Vue({
        delimiters: ['[[', ']]'],     
        el: '#Axiomas',
        data: {
            axiomas: [],
            csrfmiddlewaretoken: "",
        },

        methods: 
        {
            add: function()
            {
                this.axiomas.push({ input: '', output: '', edit_mode: false});
            },
            del: function(index)
            {
                this.axiomas.splice(index, 1);
            },
            send: function(index)
            {
                //alert("HEAR")
                //alert(this.csrfmiddlewaretoken)
                var self = this
                var i = index
                $.ajax({
                  url: "http://127.0.0.1:8000/axioma_generator/send_formula",
                  type: "POST",
                  data: {formula_string: this.axiomas[index].input, csrfmiddlewaretoken: this.csrfmiddlewaretoken},
                  success: function (data) 
                  {//возвращаемый результат от сервера
                    //alert(data.output)
                    //alert(self.axiomas[i].input)
                    self.axiomas[i].output = data.output
                    alert("SUCCESS")
                  },
                  error: function(data){
                    alert("ERROR")
                  } 
                });
            }
        },
        created: function () 
        {   
            this.csrfmiddlewaretoken = document.getElementsByClassName('csrfmiddlewaretoken').item(0).value;
        }
    })

});

'use strict'; 
$(window).on("load", function() {	
    

 //PRELOADER
 $('#preloader').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.

}); // window load end 


    
    $(document).on("ready", function() {	
        
        
    

    
        
    //NAVBAR SHOW - HIDE
    if ($('.home').length) {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            var homeheight = $(".home").height() - 73;
            if (scroll > homeheight) {
                $("header").addClass("nav-fixed");
            }
            else {
                $("header").removeClass("nav-fixed");
            }
        });
    }
    
    
    // HOME PAGE HEIGHT
    if ($('.home').length) {
        function centerInit() {
            var hometext = $('.home')
            hometext.css({
                "height": $(window).height() + "px"
            });
        }
        centerInit();
        $(window).resize(centerInit);
    }
    
    
    //SLIDE MENU
    if ($('.right-menu').length) {
    (function ($) {
        $(".right-menu").on('click', function () {
            $("body").hasClass("slidemenu-opened") ? k() : T()
        });
    })(jQuery);

    function T() {
        $("body").addClass("slidemenu-opened")
    }

    function k() {
        $("body").removeClass("slidemenu-opened")
    }
    } 
}); // document ready end 


from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings

class IndexView(TemplateView):
    """Главная страница сайта."""
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['STATIC_URL'] = settings.STATIC_URL
        context['user'] = self.request.user
        print(self.request.user)
        return context

    def post(self, request, *args, **kwargs):
        print(request.POST)
        context = self.get_context_data(**kwargs)
        context["jopka"] = "JOPA"
        return self.render_to_response(context)

from django.apps import AppConfig


class AxiomaGeneratorConfig(AppConfig):
    name = 'axioma_generator'

from django.shortcuts import render
from django.views.generic import TemplateView
from django.conf import settings
from django.http import JsonResponse


# Create your views here.
class AxiomaGenerator(TemplateView):
    """Главная страница сайта."""
    template_name = 'axioma_generator.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['STATIC_URL'] = settings.STATIC_URL
        context['user'] = self.request.user
        print(self.request.user)
        return context

    def post(self, request, *args, **kwargs):
        print(request.POST)
        context = self.get_context_data(**kwargs)
        context["jopka"] = "JOPA"
        return self.render_to_response(context)


def get_formula(request):
    print(request.POST)
    return JsonResponse({'output': 'something'}, content_type = 'application/json')